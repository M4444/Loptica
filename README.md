# Ball / Loptica
Ball (Loptica in Serbian) is a breakout-style game with a different outlook.
There in no bottomless pit so you will finish the game (in 6m 23s) even it you don't do anything.
This means that the only objective here is to find the best way to finish the game as fast (or as slow) as you can.

The ball speeds up when it hits the red border and slows down when it hits the orange sides of the platform.

~ Have fun! ~

### Download here:
- [English version](https://gitlab.com/M4444/Loptica/uploads/17951ba035e907b9c6bfc5ce9bbcbc62/Ball_game.jar)
- [Serbian version](https://gitlab.com/M4444/Loptica/uploads/d588a0f9ae30657b35130892e6e899fb/Loptica_igra.jar)

Checkout code in Serbian on the [srpski](https://gitlab.com/M4444/Loptica/tree/srpski) branch.

### How it looks:
![](assets/img/game.png?raw=true)
